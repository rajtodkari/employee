package com.micro.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class EmployeeBoot {

	@Autowired
	private Environment env;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpringApplication.run(EmployeeBoot.class);
	}

}
