package com.micro.services.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.micro.services.model.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String> {

}
